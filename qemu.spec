Summary: QEMU user mode emulation of qemu targets static build
Name: qemu-user-static
Version: 2.10.0
Release: 4%{?dist}
Epoch: 2
License: GPLv2+ and LGPLv2+ and BSD
Group: Development/Tools
URL: http://www.qemu.org/

Source0: http://wiki.qemu-project.org/download/qemu-%{version}.tar.xz

Source1: qemu.binfmt

BuildRequires: gcc

BuildRequires: flex
BuildRequires: bison
# For acpi compilation
#BuildRequires: iasl
# For chrpath calls in specfile
BuildRequires: chrpath

# used in various places for compression
BuildRequires: zlib-devel
# snappy compression for memory dump
#BuildRequires: snappy-devel
# lzo compression for memory dump
#BuildRequires: lzo-devel
# used by 9pfs
#BuildRequires: libattr-devel
#BuildRequires: libcap-devel
# seccomp containment support
BuildRequires: libseccomp-devel >= 2.3.0
# For uuid generation
BuildRequires: libuuid-devel
# Hard requirement for version >= 1.3
BuildRequires: pixman-devel
# RDMA migration
%ifnarch s390 s390x
BuildRequires: librdmacm-devel
%endif
%ifarch %{ix86} x86_64 aarch64
# qemu 2.1: needed for memdev hostmem backend
BuildRequires: numactl-devel
%endif
# qemu 2.3: reading bzip2 compressed dmg images
#BuildRequires: bzip2-devel
# qemu 2.5: needed for TLS test suite
#BuildRequires: libtasn1-devel
# qemu 2.5: libcacard is it's own project now
#BuildRequires: libcacard-devel >= 2.5.0

BuildRequires: glibc-static pcre-static glib2-static zlib-static
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
# qemu-user-binfmt + qemu-user-static both provide binfmt rules
Conflicts: qemu-user-binfmt
Provides: qemu-user-binfmt

%description
QEMU is a generic and open source processor emulator which achieves a good
emulation speed by using dynamic translation.

This package provides the user mode emulation of qemu targets built as
static binaries

%prep
%setup -q -n qemu-%{version}
%autopatch -p1

%build

# QEMU already knows how to set _FORTIFY_SOURCE
%global optflags %(echo %{optflags} | sed 's/-Wp,-D_FORTIFY_SOURCE=2//')

# drop -g flag to prevent memory exhaustion by linker
%ifarch s390
%global optflags %(echo %{optflags} | sed 's/-g//')
sed -i.debug 's/"-g $CFLAGS"/"$CFLAGS"/g' configure
%endif

# OOM killer breaks builds with parallel make on s390(x)
%ifarch s390 s390x
%global _smp_mflags %{nil}
%endif


# --build-id option is used for giving info to the debug packages.
extraldflags="-Wl,--build-id";
buildldflags="VL_LDFLAGS=-Wl,--build-id"

user_arch="\
  aarch64 \
  ppc64le \
  s390x \
  x86_64"

static_targets=

for arch in $user_arch
do
  static_targets="$static_targets $arch-linux-user"
done

run_configure() {
    ../configure \
        --prefix=%{_prefix} \
        --libdir=%{_libdir} \
        --sysconfdir=%{_sysconfdir} \
        --interp-prefix=%{_prefix}/qemu-%%M \
        --localstatedir=%{_localstatedir} \
        --libexecdir=%{_libexecdir} \
        --with-pkgversion=qemu-%{version}-%{release} \
        --disable-strip \
        --disable-werror \
        --enable-kvm \
%ifarch s390 %{mips64}
        --enable-tcg-interpreter \
%endif
        "$@" || cat config.log
}

mkdir build-static
pushd build-static

run_configure \
%ifnarch aarch64
    --extra-ldflags="$extraldflags -Wl,-z,relro -Wl,-z,now" \
%else
    --extra-ldflags="$extraldflags" \
%endif
    --extra-cflags="%{optflags}" \
    --target-list="$static_targets" \
    --static \
    --disable-pie \
    --disable-tcmalloc \
    --disable-sdl \
    --disable-gtk \
    --disable-spice \
    --disable-tools \
    --disable-guest-agent \
    --disable-guest-agent-msi \
    --disable-curses \
    --disable-curl \
    --disable-gnutls \
    --disable-gcrypt \
    --disable-nettle \
    --disable-cap-ng \
    --disable-brlapi \
    --disable-libnfs \
    --disable-blobs \
    --disable-vnc \
    --disable-docs

make V=1 %{?_smp_mflags} $buildldflags

popd

%install
pushd build-static
install -d -m 0755 %{buildroot}%{_bindir}

for arch in x86_64 aarch64 ppc64le s390x
do
  install -c -m 0755 ${arch}-linux-user/qemu-${arch} %{buildroot}%{_bindir}/qemu-${arch}-static
done
popd

# Install binfmt
mkdir -p %{buildroot}%{_exec_prefix}/lib/binfmt.d
for i in dummy \
%ifnarch %{ix86} x86_64
    qemu-i386 \
%endif
%ifnarch aarch64
    qemu-aarch64 \
%endif
%ifnarch ppc %{power64}
    qemu-ppc64le \
%endif
%ifnarch s390 s390x
    qemu-s390x \
%endif
; do
  test $i = dummy && continue

grep /$i:\$ %{_sourcedir}/qemu.binfmt | tr -d '\n' > %{buildroot}%{_exec_prefix}/lib/binfmt.d/$i-static.conf
perl -i -p -e "s/$i:\$/$i-static:F/" %{buildroot}%{_exec_prefix}/lib/binfmt.d/$i-static.conf
chmod 644 %{buildroot}%{_exec_prefix}/lib/binfmt.d/$i-static.conf

done < %{_sourcedir}/qemu.binfmt

# When building using 'rpmbuild' or 'fedpkg local', RPATHs can be left in
# the binaries and libraries (although this doesn't occur when
# building in Koji, for some unknown reason). Some discussion here:
#
# https://lists.fedoraproject.org/pipermail/devel/2013-November/192553.html
#
# In any case it should always be safe to remove RPATHs from
# the final binaries:
for f in %{buildroot}%{_bindir}/* %{buildroot}%{_libdir}/* \
         %{buildroot}%{_libexecdir}/*; do
  if file $f | grep -q ELF | grep -q -i shared; then chrpath --delete $f; fi
done

%post
/bin/systemctl --system try-restart systemd-binfmt.service &>/dev/null || :
%postun
/bin/systemctl --system try-restart systemd-binfmt.service &>/dev/null || :

%files
%{_exec_prefix}/lib/binfmt.d/qemu-*-static.conf
%{_bindir}/qemu-*-static

%changelog
* Wed Feb 19 2020 Ben Morrice <ben.morrice@cern.ch> - 2:2.10.0-4
- add F flag to binfmt, requires ml kernel when running on el7

* Wed Sep 27 2017 Jason DeTiberus <jdetiber@redhat.com> - 2:2.10.0-3.0
- Only build qemu-user-static instead of all qemu subpackages
- Fix binfmt config for ppc64le
- Remove F flag from binfmt, since RHEL7 kernel does not support it

* Fri Sep 22 2017 Paolo Bonzini <pbonzini@redhat.com> - 2:2.10.0-3
- Backport persistent reservation manager in preparation for SELinux work
- Fix previous patch

* Mon Sep 18 2017 Nathaniel McCallum <npmccallum@redhat.com> - 2:2.10.0-2
- Fix endianness of e_type in the ppc64le binfmt

* Thu Sep 07 2017 Cole Robinson <crobinso@redhat.com> - 2:2.10.0-1
- Rebase to 2.10.0 GA

* Tue Aug 29 2017 Nathaniel McCallum <npmccallum@redhat.com> - 2:2.10.0-0.5.rc4
- Fix incorrect byte order in e_machine field in ppc64le binfmt (#1486379)

* Fri Aug 25 2017 Cole Robinson <crobinso@redhat.com> - 2:2.10.0-0.4.rc4
- Rebase to 2.10.0-rc4

* Tue Aug 22 2017 Adam Williamson <awilliam@redhat.com> - 2:2.10.0-0.3.rc3
- Don't build against rdma on 32-bit ARM (#1484155)

* Wed Aug 16 2017 Cole Robinson <crobinso@redhat.com> - 2:2.10.0-0.2.rc3
- Rebase to 2.10.0-rc3

* Thu Aug 03 2017 Cole Robinson <crobinso@redhat.com> - 2:2.10.0-0.1.rc1
- Rebase to 2.10.0-rc1

* Sun Jul 30 2017 Florian Weimer <fweimer@redhat.com> - 2:2.9.0-9
- Rebuild with binutils fix for ppc64le (#1475636)

* Tue Jul 25 2017 Daniel Berrange <berrange@redhat.com> - 2:2.9.0-8
- Disabled RBD on arm & ppc64 (rhbz #1474743)

* Thu Jul 20 2017 Nathaniel McCallum <npmccallum@redhat.com> - 2:2.9.0-7
- Fix binfmt dependencies and post scriptlets
- Add binfmt for ppc64le

* Wed Jul 19 2017 Daniel Berrange <berrange@redhat.com> - 2:2.9.0-6
- Fixes for compat with Xen 4.9

* Tue Jul 18 2017 Nathaniel McCallum <npmccallum@redhat.com> - 2:2.9.0-5
- Fix ucontext_t references

* Tue Jul 18 2017 Daniel P. Berrange <berrange@redhat.com> - 2:2.9.0-4
- Rebuild for changed Xen sonames

* Wed Jul 12 2017 Cole Robinson <crobinso@redhat.com> - 2:2.9.0-3
- CVE-2017-8112: vmw_pvscsi: infinite loop in pvscsi_log2 (bz #1445622)
- CVE-2017-8309: audio: host memory lekage via capture buffer (bz #1446520)
- CVE-2017-8379: input: host memory lekage via keyboard events (bz #1446560)
- CVE-2017-8380: scsi: megasas: out-of-bounds read in megasas_mmio_write (bz
  #1446578)
- CVE-2017-7493: 9pfs: guest privilege escalation in virtfs mapped-file mode
  (bz #1451711)
- CVE-2017-9503: megasas: null pointer dereference while processing megasas
  command (bz #1459478)
- CVE-2017-10806: usb-redirect: stack buffer overflow in debug logging (bz
  #1468497)
- CVE-2017-9524: nbd: segfault due to client non-negotiation (bz #1460172)
- CVE-2017-10664: qemu-nbd: server breaks with SIGPIPE upon client abort (bz
  #1466192)

* Mon May 22 2017 Richard W.M. Jones <rjones@redhat.com> - 2:2.9.0-2
- Bump release and rebuild to try to fix _ZdlPvm symbol (see RHBZ#1452813).

* Tue Apr 25 2017 Cole Robinson <crobinso@redhat.com> - 2:2.9.0-1
- Rebase to qemu-2.9.0 GA

* Thu Apr 13 2017 Cole Robinson <crobinso@redhat.com> - 2:2.9.0-0.2-rc4
- Rebase to qemu-2.9.0-rc4
- Fix ipxe rom links for aarch64

* Sat Apr 08 2017 Richard W.M. Jones <rjones@redhat.com> - 2:2.9.0-0.2-rc3
- Backport upstream fix for assertion when copy-on-read=true (RHBZ#1439922).

* Tue Apr 04 2017 Cole Robinson <crobinso@redhat.com> - 2:2.9.0-0.1-rc3
- Rebase to qemu-2.9.0-rc3

* Wed Mar 29 2017 Cole Robinson <crobinso@redhat.com> - 2:2.9.0-0.1-rc2
- Rebase to qemu-2.9.0-rc2
- Add Obsoletes for or32-or1k rename (bz 1435016)
- spec: Pull in vga and pxe roms for ppc64 (bz 1431403)

* Tue Mar 21 2017 Cole Robinson <crobinso@redhat.com> - 2:2.9.0-0.1-rc1
- Rebase to qemu-2.9.0-rc1

* Wed Mar 15 2017 Cole Robinson <crobinso@redhat.com> - 2:2.9.0-0.1-rc0
* Rebase to qemu-2.9.0-rc0

* Mon Feb 20 2017 Daniel Berrange <berrange@redhat.com> - 2:2.8.0-2
- Drop texi2html BR, since QEMU switched to using makeinfo back in 2010

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 2:2.8.0-1.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Dec 20 2016 Cole Robinson <crobinso@redhat.com> - 2:2.8.0-1
- Rebase to qemu-2.8.0 GA

* Mon Dec 12 2016 Cole Robinson <crobinso@redhat.com> - 2:2.8.0-0.3-rc3
- Rebase to qemu-2.8.0-rc3

* Mon Dec 05 2016 Cole Robinson <crobinso@redhat.com> - 2:2.8.0-0.2-rc2
- Rebuild to pick up changed libxen* sonames

* Mon Dec 05 2016 Cole Robinson <crobinso@redhat.com> - 2:2.8.0-0.1-rc2
- Rebase to qemu-2.8.0-rc2

* Mon Nov 28 2016 Paolo Bonzini <pbonzini@redhat.com> - 2:2.7.0-10
- Do not build aarch64 with -fPIC anymore (rhbz 1232499)

* Tue Nov 15 2016 Nathaniel McCallum <npmccallum@redhat.com> - 2:2.7.0-9
- Clean up binfmt.d configuration files

* Mon Nov 14 2016 Richard W.M. Jones <rjones@redhat.com> - 2:2.7.0-8
- Create subpackages for modularized qemu block drivers (RHBZ#1393688).
- Fix qemu-sanity-check.

* Tue Oct 25 2016 Cole Robinson <crobinso@redhat.com> - 2:2.7.0-7
- Fix PPC64 build with memlock file (bz #1387601)

* Wed Oct 19 2016 Bastien Nocera <bnocera@redhat.com> - 2:2.7.0-6
- Add "F" flag to static user emulators' binfmt, to make them
  available in containers (#1384615)
- Also fixes the path of those emulators in the binfmt configurations

* Wed Oct 19 2016 Cole Robinson <crobinso@redhat.com> - 2:2.7.0-5
- Fix nested PPC 'Unknown MMU model' error (bz #1374749)
- Fix flickering display with boxes + wayland VM (bz #1266484)
- Add ppc64 kvm memlock file (bz #1293024)

* Sat Oct 15 2016 Cole Robinson <crobinso@redhat.com> - 2:2.7.0-4
- CVE-2016-7155: pvscsi: OOB read and infinite loop (bz #1373463)
- CVE-2016-7156: pvscsi: infinite loop when building SG list (bz #1373480)
- CVE-2016-7156: pvscsi: infinite loop when processing IO requests (bz
  #1373480)
- CVE-2016-7170: vmware_vga: OOB stack memory access (bz #1374709)
- CVE-2016-7157: mptsas: invalid memory access (bz #1373505)
- CVE-2016-7466: usb: xhci memory leakage during device unplug (bz #1377838)
- CVE-2016-7423: scsi: mptsas: OOB access (bz #1376777)
- CVE-2016-7422: virtio: null pointer dereference (bz #1376756)
- CVE-2016-7908: net: Infinite loop in mcf_fec_do_tx (bz #1381193)
- CVE-2016-8576: usb: xHCI: infinite loop vulnerability (bz #1382322)
- CVE-2016-7995: usb: hcd-ehci: memory leak (bz #1382669)

* Mon Oct 10 2016 Hans de Goede <hdegoede@redhat.com> - 2:2.7.0-3
- Fix interrupt endpoints not working with network/spice USB redirection
  on guest with an emulated xhci controller (rhbz#1382331)

* Tue Sep 20 2016 Michal Toman <mtoman@fedoraproject.org> - 2:2.7.0-2
- Fix build on MIPS

* Thu Sep 08 2016 Cole Robinson <crobinso@redhat.com> - 2:2.7.0-1
- Rebase to qemu 2.7.0 GA

* Fri Aug 19 2016 Cole Robinson <crobinso@redhat.com> - 2:2.7.0-0.2.rc3
- Rebase to qemu 2.7.0-rc3

* Wed Aug 03 2016 Cole Robinson <crobinso@redhat.com> - 2:2.7.0-0.1.rc2
- Rebase to qemu 2.7.0-rc2

* Sat Jul 23 2016 Richard W.M. Jones <rjones@redhat.com> - 2:2.6.0-6
- Rebuild to attempt to fix '2:qemu-system-xtensa-2.6.0-5.fc25.x86_64 requires libxenctrl.so.4.6()(64bit)'

* Wed Jul 13 2016 Daniel Berrange <berrange@redhat.com> - 2:2.6.0-5
- Introduce qemu-user-static sub-RPM

* Wed Jun 22 2016 Cole Robinson <crobinso@redhat.com> - 2:2.6.0-4
- CVE-2016-4002: net: buffer overflow in MIPSnet (bz #1326083)
- CVE-2016-4952 scsi: pvscsi: out-of-bounds access issue
- CVE-2016-4964: scsi: mptsas infinite loop (bz #1339157)
- CVE-2016-5106: scsi: megasas: out-of-bounds write (bz #1339581)
- CVE-2016-5105: scsi: megasas: stack information leakage (bz #1339585)
- CVE-2016-5107: scsi: megasas: out-of-bounds read (bz #1339573)
- CVE-2016-4454: display: vmsvga: out-of-bounds read (bz #1340740)
- CVE-2016-4453: display: vmsvga: infinite loop (bz #1340744)
- CVE-2016-5126: block: iscsi: buffer overflow (bz #1340925)
- CVE-2016-5238: scsi: esp: OOB write (bz #1341932)
- CVE-2016-5338: scsi: esp: OOB r/w access (bz #1343325)
- CVE-2016-5337: scsi: megasas: information leakage (bz #1343910)
- Fix crash with -nodefaults -sdl (bz #1340931)
- Add deps on edk2-ovmf and edk2-aarch64

* Thu May 26 2016 Cole Robinson <crobinso@redhat.com> - 2:2.6.0-3
- CVE-2016-4020: memory leak in kvmvapic.c (bz #1326904)
- CVE-2016-4439: scsi: esb: OOB write #1 (bz #1337503)
- CVE-2016-4441: scsi: esb: OOB write #2 (bz #1337506)
- Fix regression installing windows 7 with qxl/vga (bz #1339267)
- Fix crash with aarch64 gic-version=host and accel=tcg (bz #1339977)

* Fri May 20 2016 Cole Robinson <crobinso@redhat.com> - 2:2.6.0-2
- Explicitly error if spice GL setup fails
- Fix monitor resizing with virgl (bz #1337564)
- Fix libvirt noise when introspecting qemu-kvm without hw virt

* Fri May 13 2016 Cole Robinson <crobinso@redhat.com> - 2:2.6.0-1
- Rebase to v2.6.0 GA

* Mon May 09 2016 Cole Robinson <crobinso@redhat.com> - 2:2.6.0-0.2.rc5
- Fix gtk UI crash when switching to monitor (bz #1333424)
- Fix sdl2 UI lockup lockup when switching to monitor
- Rebased to qemu-2.6.0-rc5

* Mon May 02 2016 Cole Robinson <crobinso@redhat.com> 2:2.6.0-0.2.rc4
- Rebased to version 2.6.0-rc4
- Fix test suite on big endian hosts (bz 1330174)

* Mon Apr 25 2016 Cole Robinson <crobinso@redhat.com> - 2:2.6.0-0.2.rc3
- Rebuild to pick up spice GL support

* Mon Apr 18 2016 Cole Robinson <crobinso@redhat.com> 2:2.6.0-0.1.rc3
- Rebased to version 2.6.0-rc3
- Fix s390 sysctl file install (bz 1327870)
- Adjust spice gl version check to expect F24 backported version

* Thu Apr 14 2016 Cole Robinson <crobinso@redhat.com> 2:2.6.0-0.1.rc2
- Rebased to version 2.6.0-rc2
- Fix GL deps (bz 1325966)
- Ship sysctl file to fix s390x kvm (bz 1290589)
- Fix FTBFS on s390 (bz 1326247)

* Thu Apr 07 2016 Cole Robinson <crobinso@redhat.com> - 2:2.6.0-0.1.rc1
- Rebased to version 2.6.0-rc1

* Thu Mar 17 2016 Cole Robinson <crobinso@redhat.com> - 2:2.5.0-11
- CVE-2016-2857: net: out of bounds read (bz #1309564)
- CVE-2016-2392: usb: null pointer dereference (bz #1307115)

* Thu Mar 17 2016 Cole Robinson <crobinso@redhat.com> - 2:2.5.0-10
- CVE-2016-2538: Integer overflow in usb module (bz #1305815)
- CVE-2016-2841: ne2000: infinite loop (bz #1304047)
- CVE-2016-2857 net: out of bounds read (bz #1309564)
- CVE-2016-2392 usb: null pointer dereference (bz #1307115)
- Fix external snapshot any more after active committing (bz #1300209)

* Wed Mar  9 2016 Peter Robinson <pbrobinson@fedoraproject.org> 2:2.5.0-9
- Rebuild for tcmalloc ifunc issues on non x86 arches (see rhbz 1312462)

* Tue Mar  1 2016 Paolo Bonzini <pbonzini@redhat.com> 2:2.5.0-8
- Disable xfsctl, fallocate works fine in newer kernels (bz #1305512)

* Tue Mar  1 2016 Peter Robinson <pbrobinson@fedoraproject.org> 2:2.5.0-7
- All Fedora arches have libseccomp support (ARMv7, aarch64, Power64, s390(x))

* Mon Feb 15 2016 Cole Robinson <crobinso@redhat.com> - 2:2.5.0-6
- CVE-2015-8619: Fix sendkey out of bounds (bz #1292757)
- CVE-2016-1981: infinite loop in e1000 (bz #1299995)
- Fix Out-of-bounds read in usb-ehci (bz #1300234, bz #1299455)
- CVE-2016-2197: ahci: null pointer dereference (bz #1302952)
- Fix gdbstub for VSX registers for ppc64 (bz #1304377)
- Fix qemu-img vmdk images to work with VMware (bz #1299185)

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 2:2.5.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jan 20 2016 Cole Robinson <crobinso@redhat.com> - 2:2.5.0-4
- CVE-2015-8567: net: vmxnet3: host memory leakage (bz #1289818)
- CVE-2016-1922: i386: avoid null pointer dereference (bz #1292766)
- CVE-2015-8613: buffer overflow in megasas_ctrl_get_info (bz #1284008)
- CVE-2015-8701: Buffer overflow in tx_consume in rocker.c (bz #1293720)
- CVE-2015-8743: ne2000: OOB memory access in ioport r/w functions (bz
  #1294787)
- CVE-2016-1568: Use-after-free vulnerability in ahci (bz #1297023)
- Fix modules.d/kvm.conf example syntax (bz #1298823)

* Sat Jan 09 2016 Cole Robinson <crobinso@redhat.com> - 2:2.5.0-3
- Fix virtio 9p thread pool usage
- CVE-2015-8558: DoS by infinite loop in ehci_advance_state (bz #1291309)
- Re-add dist tag

* Thu Jan 7 2016 Paolo Bonzini <pbonzini@redhat.com> - 2:2.5.0-2
- add /etc/modprobe.d/kvm.conf
- add 0001-virtio-9p-use-accessor-to-get-thread-pool.patch

* Wed Dec 23 2015 Cole Robinson <crobinso@redhat.com> 2:2.5.0-1
- Rebased to version 2.5.0

* Tue Dec 08 2015 Cole Robinson <crobinso@redhat.com> 2:2.5.0-0.1.rc3
- Rebased to version 2.5.0-rc3

* Mon Nov 30 2015 Cole Robinson <crobinso@redhat.com> 2:2.5.0-0.1.rc2
- Rebased to version 2.5.0-rc2

* Fri Nov 20 2015 Cole Robinson <crobinso@redhat.com> 2:2.5.0-0.1.rc1
- Rebased to version 2.5.0-rc1

* Wed Nov 04 2015 Cole Robinson <crobinso@redhat.com> - 2:2.4.1-1
- Rebased to version 2.4.1

* Sun Oct 11 2015 Cole Robinson <crobinso@redhat.com> - 2:2.4.0.1-2
- Rebuild for xen 4.6

* Thu Oct 08 2015 Cole Robinson <crobinso@redhat.com> - 2:2.4.0.1-1
- Rebased to version 2.4.0.1
- CVE-2015-7295: virtio-net possible remote DoS (bz #1264393)
- drive-mirror: Fix coroutine reentrance (bz #1266936)

* Mon Sep 21 2015 Cole Robinson <crobinso@redhat.com> - 2:2.4.0-4
- CVE-2015-6815: net: e1000: infinite loop issue (bz #1260225)
- CVE-2015-6855: ide: divide by zero issue (bz #1261793)
- CVE-2015-5278: Infinite loop in ne2000_receive() (bz #1263284)
- CVE-2015-5279: Heap overflow vulnerability in ne2000_receive() (bz #1263287)

* Sun Sep 20 2015 Richard W.M. Jones <rjones@redhat.com> - 2:2.4.0-3
- Fix emulation of various instructions, required by libm in F22 ppc64 guests.

* Mon Aug 31 2015 Cole Robinson <crobinso@redhat.com> - 2:2.4.0-2
- CVE-2015-5255: heap memory corruption in vnc_refresh_server_surface (bz
  #1255899)

* Tue Aug 11 2015 Cole Robinson <crobinso@redhat.com> - 2:2.4.0-1
- Rebased to version 2.4.0
- Support for virtio-gpu, 2D only
- Support for virtio-based keyboard/mouse/tablet emulation
- x86 support for memory hot-unplug
- ACPI v5.1 table support for 'virt' board

* Sun Aug 09 2015 Cole Robinson <crobinso@redhat.com> - 2:2.4.0-0.2.rc4
- CVE-2015-3209: pcnet: multi-tmd buffer overflow in the tx path (bz #1230536)
- CVE-2015-3214: i8254: out-of-bounds memory access (bz #1243728)
- CVE-2015-5158: scsi stack buffer overflow (bz #1246025)
- CVE-2015-5154: ide: atapi: heap overflow during I/O buffer memory access (bz
  #1247141)
- CVE-2015-5165: rtl8139 uninitialized heap memory information leakage to
  guest (bz #1249755)
- CVE-2015-5166: BlockBackend object use after free issue (bz #1249758)
- CVE-2015-5745: buffer overflow in virtio-serial (bz #1251160)

* Tue Jul 14 2015 Cole Robinson <crobinso@redhat.com> 2:2.4.0-0.1-rc0
- Rebased to version 2.4.0-rc0

* Fri Jul  3 2015 Richard W.M. Jones <rjones@redhat.com> - 2:2.3.0-15
- Bump and rebuild.

* Fri Jul  3 2015 Daniel P. Berrange <berrange@redhat.com> - 2:2.3.0-14
- Use explicit --(enable,disable)-spice args (rhbz #1239102)

* Thu Jul  2 2015 Peter Robinson <pbrobinson@fedoraproject.org> 2:2.3.0-13
- Build aarch64 with -fPIC (rhbz 1232499)

* Wed Jul 01 2015 Nick Clifton <nickc@redhat.com> - 2:2.3.0-12
- Disable stack protection for AArch64.  F23's GCC thinks that it is available but F23's glibc does not support it.

* Fri Jun 26 2015 Paolo Bonzini <pbonzini@redhat.com> - 2:2.3.0-10
- Rebuild for libiscsi soname bump

* Fri Jun 19 2015 Paolo Bonzini <pbonzini@redhat.com> - 2:2.3.0-10
- Re-enable tcmalloc on arm

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2:2.3.0-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Jun 10 2015 Dan Horák <dan[at]danny.cz> - 2:2.3.0-8
- gperftools not available on s390(x)

* Fri Jun 05 2015 Cole Robinson <crobinso@redhat.com> - 2:2.3.0-7
- CVE-2015-4037: insecure temporary file use in /net/slirp.c (bz #1222894)

* Mon Jun  1 2015 Daniel P. Berrange <berrange@redhat.com> - 2:2.3.0-6
- Disable tcmalloc on arm since it currently hangs (rhbz #1226806)
- Re-enable tests on arm

* Wed May 13 2015 Cole Robinson <crobinso@redhat.com> - 2:2.3.0-5
- Backport upstream 2.4 patch to link with tcmalloc, enable it
- CVE-2015-3456: (VENOM) fdc: out-of-bounds fifo buffer memory access (bz
  #1221152)

* Sun May 10 2015 Paolo Bonzini <pbonzini@redhat.com> 2:2.3.0-4
- Backport upstream 2.4 patch to link with tcmalloc, enable it
- Add -p1 to autopatch

* Wed May 06 2015 Cole Robinson <crobinso@redhat.com> 2:2.3.0-3
- Fix ksm.service (bz 1218814)

* Tue May  5 2015 Dan Horák <dan[at]danny.cz> - 2:2.3.0-2
- Require libseccomp only when built with it

* Tue Mar 24 2015 Cole Robinson <crobinso@redhat.com> - 2:2.3.0-1
- Rebased to version 2.3.0 GA
- Another attempt at fixing default /dev/kvm permissions (bz 950436)

* Tue Mar 24 2015 Cole Robinson <crobinso@redhat.com> - 2:2.3.0-0.5.rc3
- Drop unneeded kvm.modules
- Fix s390/ppc64 FTBFS (bz 1212328)

* Tue Mar 24 2015 Cole Robinson <crobinso@redhat.com> - 2:2.3.0-0.4.rc3
- Rebased to version 2.3.0-rc3

* Tue Mar 24 2015 Cole Robinson <crobinso@redhat.com> - 2:2.3.0-0.3.rc2
- Rebased to version 2.3.0-rc2
- Don't install ksm services as executable (bz #1192720)
- Skip hanging tests on s390 (bz #1206057)
- CVE-2015-1779 vnc: insufficient resource limiting in VNC websockets decoder
  (bz #1205051, bz #1199572)

* Tue Mar 24 2015 Cole Robinson <crobinso@redhat.com> - 2:2.3.0-0.2.rc1
- Rebased to version 2.3.0-rc1

* Sun Mar 22 2015 Cole Robinson <crobinso@redhat.com> - 2:2.3.0-0.1.rc0
- Rebased to version 2.3.0-rc0

* Tue Feb 17 2015 Richard W.M. Jones <rjones@redhat.com> - 2:2.2.0-7
- Add -fPIC flag to build to avoid
  'relocation R_X86_64_PC32 against undefined symbol' errors.
- Add a hopefully temporary hack so that -fPIC is used to build
  NSS files in libcacard.

* Wed Feb  4 2015 Richard W.M. Jones <rjones@redhat.com> - 2:2.2.0-5
- Add UEFI support for aarch64.

* Tue Feb  3 2015 Daniel P. Berrange <berrange@redhat.com> - 2:2.2.0-4
- Re-enable SPICE after previous build fixes circular dep

* Tue Feb  3 2015 Daniel P. Berrange <berrange@redhat.com> - 2:2.2.0-3
- Rebuild for changed xen soname
- Temporarily disable SPICE to break circular build-dep on libcacard
- Stop libcacard linking against the entire world

* Wed Jan 28 2015 Daniel P. Berrange <berrange@redhat.com> - 2:2.2.0-2
- Pass package information to configure
