# qemu-user-static

**This is not used by C8/CS8 machines, you're looking for [qemu](https://gitlab.cern.ch/linuxsupport/rpms/qemu).**

* CERN build of qemu-user-static for EL7 hosts to enable cross compilation of aarch64 via emulation through mock/koji
	* Requires a kernel with this patch: https://github.com/torvalds/linux/commit/948b701a607f123df92ed29084413e5dd8cda2ed
	* This will most likely be `yum  --enablerepo="elrepo-kernel" install kernel-ml` on el7

* The following should be seen if working correctly (crucially the F flag should be present):
```
[root@host ~]# cat /proc/sys/fs/binfmt_misc/qemu-aarch64 
enabled
interpreter /usr/bin/qemu-aarch64-static
flags: F
offset 0
magic 7f454c460201010000000000000000000200b700
mask ffffffffffffff000000000000000000feffffff
```
